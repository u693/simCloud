# Web Supplementary Material

This README contains additional tests from the paper titled "Harnessing Cloud Resources for Efficient Execution of High-Performance Simulations".

## Section 1: Checkpoint Overhead Evaluation

This section describes the tests conducted to evaluate checkpoint overhead, focusing on storage and cache infrastructure. The main objective is to evaluate the dump time of checkpointing in the regions SA-East-1 and US-East-1 and estimate the $Overhead^{CKP}_{US-EAST-1}$ parameter. This value is necessary for the following purposes:

1. Conducting new tests in the new environment.
2. Updating the selection algorithm to include new checkpoint times.
3. Assessing the impact of cache on dump time.

For these tests, we used the c5.24xlarge on-demand VM in both regions. An on-demand VM was chosen to prevent spot revocation during the tests. We adhered to a 30-minute interval and utilized the PreSal short model.

All tests were performed in the `/cachetc/` folder, acting as a transparent mount point to the cache: access from SA-East-1 means the VM accesses storage in SA-East-1, and vice versa.

![Checkpoint times](images/ckp_test.png)

**Figure 1: Average dump time considering the PreSal-Short model.**

According to results presented in Figure 1, the dump time averages 13 seconds ($DUMP = 13s$, considering the closest region), while the $OVERHEAD_{US-EAST-1}^{CKP} = 2.2$.

Additionally, we also conducted a comparison between dump times without cache use and with the cache solution.

![Checkpoint times](images/ckp_test_cache.png)

**Figure 2: Impact of the cache solution on checkpoint dump time.**

Therefore, as depicted in Figure 2, the cache significantly reduced dump time, especially for the US-EAST-1 region.

## Section 2: Defining the Read Bandwidth

A critical parameter for the selection algorithm is read bandwidth, i.e., the available bandwidth when reading a file from the cache.

To estimate this bandwidth, we conducted a test using IOR, an I/O microbenchmark. IOR was used to read a 10GB file from the cache running on a VM allocated in each region.

First, we generated a 10GB file in the cache from the headnode. To generate this file, we navigated to the cache folder and used the following command:

```bash
dd if=/dev/urandom of=testfile4  bs=1M count=10240
10240+0 records in
10240+0 records out
10737418240 bytes (11 GB, 10 GiB) copied, 258.042 s, 41.6 MB/s
```

Note: The `dd` command also returns a bandwidth measurement, which we include here for curiosity.

Next, we allocated a VM in the US-East-1 region and executed IOR. We followed the same procedure for the SA-East-1 region, using the c5.24xlarge VM for the tests.

The file was read three times (consecutive readings), and these metrics were reported by IOR.

#### SA-EAST-1

| Execution | Bandwidth (MiB/s) | Latency (s) |
|-----------|-------------------|-------------|
| 1         | 253.24            | 0.029097    |
| 2         | 32508             | 0.000185    |
| 3         | 53817             | 0.000144    |

#### US-EAST-1

| Execution | Bandwidth (MiB/s) | Latency (s) |
|-----------|-------------------|-------------|
| 1         | 55.13             | 0.143303    |
| 2         | 33886             | 0.000180    |
| 3         | 54640             | 0.000140    |

Based on these results, the bandwidth used in the manuscript's reported tests was $Bandwidth_{SA-EAST-1} = 250MB/s$ and $Bandwidth_{US-EAST-1} = 55MB/s$. Note that these bandwidths represent the first operation in the cache, i.e., before the cache was populated, and therefore do not reflect the cache's impact on I/O performance.

## Section 3: Cache Evaluation Using Presal Execution

In these tests, we evaluated the cache using the Presal model. The goal was to determine the impact of the cache on the application's execution time. We used one on-demand VM, with the frequency of the checkpoint set at 30 minutes.

![cache evaluation](images/cache_presal.png)

**Figure 3: Impact of the Cache Solution considering the Presal model.**